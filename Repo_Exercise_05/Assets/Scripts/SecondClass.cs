﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class SecondClass : MonoBehaviour
    {
        public bool isGrounded;
        public bool canJump;

        void Update()
        {
            if(isGrounded)
            {
                canJump = true;
            }
            else
            {
                canJump = false;
            }

            Debug.Log("Player can jump: " + canJump);
        }
    }
}