﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class FirstClass : MonoBehaviour
    {
        public bool isALive;
        public int health = 100;

        void Update()
        {
            if(isALive)
            {
                Debug.Log("Entity is alive.");
                if (health <= 10)
                {
                    Debug.Log("But the health is low.");
                }
            }
        }
    }
}