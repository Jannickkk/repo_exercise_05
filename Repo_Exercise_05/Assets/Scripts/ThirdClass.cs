﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class ThirdClass : MonoBehaviour
    {
        public bool isRed;
        public bool isBlue;
        public bool isYellow;

        private void CheckColorMix()
        {
            if(isRed && isBlue && isYellow)
            {
                Debug.Log("Three colors are mixed.");
            }
            else if(isRed && isBlue || isBlue && isBlue || isYellow && isRed)
            {
                Debug.Log("Two colors are mixed.");
            }
            else
            {
                Debug.Log("No colors are mixed.");
            }
        }

        private void CheckColors()
        {
            if(isRed || isBlue || isYellow)
            {
                Debug.Log("There is at least one color.");
            }
        }

        void Update()
        {
            CheckColors();
            CheckColorMix();
        }
    }
}
