﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class FourthCLass : MonoBehaviour
    {
        public PlayerState playerState;
        private float movementSpeed;

        private void Update()
        {
            switch (playerState)
            {
                case PlayerState.NONE:

                    movementSpeed = 0;
                    Debug.LogError("NO STATE SET - THIS IS AN ERROR");
                    break;
                case PlayerState.IDLE:
                    movementSpeed = 1;
                    Debug.Log("Player is taking a stroll. Speed: " + movementSpeed);
                    break;
                case PlayerState.WALKING:
                    movementSpeed = 2;
                    Debug.Log("Player is walking around. Speed: " + movementSpeed);
                    break;
                case PlayerState.RUNNING:
                    movementSpeed = 4;
                    Debug.Log("Player is running lightning fast!. Speed: " + movementSpeed);
                    break;
            }
        }
    }
}
