﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public enum PlayerState
    {
        NONE,
        IDLE,
        WALKING,
        RUNNING
    }
}
